'''miniEP1
Thiago Guerrero, 11275297
Para executar:
$> make lenta
'''
def main():
    n = 1
    primos = 0
    primosEspeciais = 0
    crivo = []

    n = int(input())
    n = 2**n
    for i in range(n+1):
        crivo.append(True)

    for i in range(2, n+1):
        if (crivo[i]):
            primos += 1
            if ((i % 4) != 3):
                primosEspeciais += 1
            for j in range(i*2, n+1, i):
                crivo[j] = False
    print ("Primos:", primos)
    print ("Primos Especiais:", primosEspeciais)
main()