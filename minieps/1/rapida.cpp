/* miniEP1
Thiago Guerrero, 11275297
Para compilar:
$> make rapida
Para executar:
$> ./rapida
*/

#include <iostream>
#include <math.h>
using namespace std;

int main() {
    unsigned int n;
    int primos = 0, primosEspeciais = 0;
    bool *crivo;
    cin >> n;
    n = pow(2, n);
    
    crivo = (bool *)malloc((n + 1) * sizeof(bool));
    for (int i = 2; i <= n; i++)
        crivo[i] = true;

    for (int i = 2; i <= n; i++){
        if (crivo[i]) {
            primos++;
            if (i % 4 != 3)
                primosEspeciais++;
            for (int j = i*2; j <= n; j += i)
                crivo[j] = false;
        }
    }

    cout<<"Primos: "<<primos<<endl;
    cout<<"Primos Especiais: "<<primosEspeciais<<endl;
    return 0;
}
