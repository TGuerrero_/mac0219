import sys
import numpy as np
from subprocess import Popen, PIPE

def main():
  path = sys.argv[1]
  n = int(sys.argv[2])

  correct = n
  ground_truth = np.arange(100)
  ground_truth = np.array2string(ground_truth).strip('[]').replace(' ', '').replace('\n', '')
  for i in range(n):
    permutation = np.random.permutation(100).astype(str)

    process = Popen(np.insert(permutation, 0, path), stdout=PIPE)
    stdout = str(process.stdout.readline())
    result = stdout[2:-4] # Remove strange chars
    result = result.replace(' ', '') 

    if (ground_truth != result):
      print("Should result:", ground_truth)
      print("Result: ", result)
      correct = correct - 1
      if (correct/n < 0.9):
        print("Not Ok")
        return
    else:
      print("Deu bom")

  print("Ok")

if __name__ == "__main__":
  main()