#!/bin/bash
rm -f log.txt

TYPES=("rapida" "lenta")
INPUTS=(24 26)

for i in {1..5}; do
    echo "###### Medida ${i} ######" >> log.txt
    for type in "${TYPES[@]}"; do
        for input in "${INPUTS[@]}"; do
            if [[ ${type} == "rapida" ]]; then
                echo "C++ - input: ${input}" >> log.txt
                echo ${input} | /usr/bin/time ./rapida 2>> log.txt
            else
                echo "Python - input: ${input}" >> log.txt
                echo ${input} | /usr/bin/time python3 lenta.py 2>> log.txt
            fi
            printf "\n" >> log.txt
        done
    done
    printf "\n" >> log.txt
done
