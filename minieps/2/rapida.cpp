/* miniEP1
Thiago Guerrero, 11275297
Para compilar:
$> make rapida
Para executar:
$> ./rapida
*/

#include <iostream>
#include <math.h>
#include <string.h>
#include <bitset>
using namespace std;

int main() {
    unsigned int n;
    int primos = 1, primosEspeciais = 1;
    bool *crivo;
    cin >> n;
    n = 1 << n;

    crivo = (bool*)malloc((n + 1)/2 * sizeof(bool));
    memset(crivo + 1, true, (n+1-2)/2*sizeof(bool));

    for (int i = 3; i <= n; i+=2){
        if (crivo[i/2]) {
            primos++;
            if (i % 4 != 3)
                primosEspeciais++;
            for (int j = 3*i; j <= n; j += 2*i)
                crivo[j/2] = false;
        }
    }

    cout<<"Primos: "<<primos<<endl;
    cout<<"Primos Especiais: "<<primosEspeciais<<endl;
    return 0;
}
