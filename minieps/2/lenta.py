'''miniEP1
Thiago Guerrero, 11275297
Para executar:
$> make lenta
'''
def main():
    n = 1 << int(input())
    primos = 1
    primosEspeciais = 1
    crivo = [True]*((n+1)//2)

    for i in range(3, n+1, 2):
        if (crivo[i//2]):
            primos += 1
            if ((i % 4) != 3):
                primosEspeciais += 1
            for j in range(3*i, n+1, 2*i):
                crivo[j//2] = 0
    print ("Primos:", primos)
    print ("Primos Especiais:", primosEspeciais)
main()