import numpy as np

n = int(input("Digite a quantidade de números: "))
numbers = np.array([], dtype=float)

for i in range(n):
  numbers = np.append(numbers, float(input()))

print("Média: ", np.mean(numbers))
print("Variância: ", np.var(numbers, ddof=1))