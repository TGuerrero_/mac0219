#!/bin/bash
rm -f log.txt
touch log.txt
cd ./lua-5.4.3-modificado

/usr/bin/time make -B 2> ../log.txt

for i in {1..5}; do
  echo "###### Medida $i ######" >> ../log.txt
  /usr/bin/time ./src/lua bench.lua 2>> ../log.txt
  printf "\n" >> ../log.txt
done