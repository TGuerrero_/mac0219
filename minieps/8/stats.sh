#!/bin/bash
INPUT=./input
OUTPUT=./log
make
precision=3
sum=0
for i in {1..10}; do
  echo "Iteration $i"
  # Redireciona stdout pro /dev/null e pega o stderr no stdout
  result=$(/usr/bin/time -f "%e" ./bs ${INPUT} ${OUTPUT} 2>&1 > /dev/null)
  sum=$(bc <<< "$sum + $result")
done

echo $(bc <<< "scale=$precision; $sum/10")